 package nl.utwente.di.bookQuote;
    import org.junit.jupiter.api.Assertions;
    import org.junit.jupiter.api.Test;
    /**
     * Tests the Quoter
    */
    public class TestQuote {
        @Test
        public void testBook1( )throws Exception{
            Quoter quoter = new Quoter ( );
            double price = quoter.getBookPrice("2" );
            Assertions.assertEquals( 45.0,price,0.0,"Priceofbook1");
        }
    }
