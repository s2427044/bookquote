package nl.utwente.di.bookQuote;
import java.util.HashMap;

public class Quoter {
    HashMap<String, Double> Price =  new HashMap<String,Double>();
    public Quoter() {
        Price.put("1",10.0);
        Price.put("2",45.0);
        Price.put("3",20.0);
        Price.put("4",35.0);
        Price.put("5",50.0);
    }
    public double getBookPrice(String isbn) {
        if (Price.containsKey(isbn)) {
            return Price.get(isbn);
        }
        return 0;
    }
}
